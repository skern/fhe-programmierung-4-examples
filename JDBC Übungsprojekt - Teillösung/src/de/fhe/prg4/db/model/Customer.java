package de.fhe.prg4.db.model;

public class Customer extends Person
{
    private String preferences;

    /*
        Simpler Konstruktor
     */
    public Customer()
    {
        super();
        this.preferences = "Unknown Customer Preferences";
    }

    public Customer( String lastname, String firstname, String preferences )
    {
        super( lastname, firstname );
        this.preferences = preferences;
    }

    /*
        Getter & Setter
     */

    public String getPreferences()
    {
        return preferences;
    }

    public void setPreferences(String preferences)
    {
        this.preferences = preferences;
    }

    /*
        Helper Methods
     */

    @Override
    public String toString()
    {
        return  "Customer: " + super.toString() +
                "\n\tPreferences: " + this.getPreferences();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (!super.equals(o)) return false;

        Customer customer = (Customer) o;

        if (!preferences.equals(customer.preferences)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        result = 31 * result + preferences.hashCode();

        return result;
    }
}
