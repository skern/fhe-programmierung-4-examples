package de.fhe.prg4.db.model;

import java.util.ArrayList;
import java.util.List;

public class Division
{
    private String name;
    private Executive lead;
    private List<Employee> members;

    public Division()
    {
        this.name = "Unknown Division";
        this.lead = null;
        this.members = new ArrayList<Employee>();
    }

    public Division( String name, Executive lead )
    {
        this();
        this.name = name;
        this.lead = lead;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Executive getLead()
    {
        return lead;
    }

    public void setLead(Executive lead)
    {
        this.lead = lead;
    }

    public List<Employee> getMembers()
    {
        return members;
    }

    public void setMembers(List<Employee> members)
    {
        this.members = members;
    }
}
