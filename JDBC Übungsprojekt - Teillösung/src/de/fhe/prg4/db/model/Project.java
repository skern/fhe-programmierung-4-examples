package de.fhe.prg4.db.model;

import java.util.ArrayList;
import java.util.List;

public class Project
{
    private String name;
    private List<Employee> team;

    public Project()
    {
        this.name = "Unknown Project";
        this.team = new ArrayList<Employee>();
    }

    public Project(String name)
    {
        this();
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Employee> getTeam()
    {
        return team;
    }

    public void setTeam(List<Employee> team)
    {
        this.team = team;
    }
}
