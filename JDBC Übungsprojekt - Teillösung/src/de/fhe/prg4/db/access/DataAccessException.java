package de.fhe.prg4.db.access;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 03.04.14
 * Time: 22:59
 */
public class DataAccessException extends Exception
{
    public DataAccessException( String message )
    {
        super( message );
    }

    public DataAccessException(){}
}
