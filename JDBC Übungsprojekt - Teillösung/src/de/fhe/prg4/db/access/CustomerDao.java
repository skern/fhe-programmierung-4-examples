package de.fhe.prg4.db.access;

import de.fhe.prg4.db.model.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDao
{
    private static final String COLUMN_ID = "cid";
    private static final String COLUMN_FIRST_NAME = "firstname";
    private static final String COLUMN_LAST_NAME = "lastname";
    private static final String COLUMN_PREFERENCES = "pref";

    private static final String CREATE_CUSTOMER_TABLE =
            "CREATE TABLE IF NOT EXISTS Customers ( " +
                    COLUMN_ID + " INT PRIMARY KEY AUTO_INCREMENT, " +
                    COLUMN_FIRST_NAME + " VARCHAR NOT NULL, " +
                    COLUMN_LAST_NAME + " VARCHAR NOT NULL, " +
                    COLUMN_PREFERENCES + " VARCHAR NOT NULL)";

    private static final String INSERT_CUSTOMER =
            "INSERT INTO Customers (" + COLUMN_FIRST_NAME + ", " +
                                        COLUMN_LAST_NAME + ", " +
                                        COLUMN_PREFERENCES + ") VALUES ( ?, ?, ? )";

    private static final String READ_ALL_CUSTOMERS =
            "SELECT * from Customers";

    private Connection connection;


    public CustomerDao() throws DataAccessException
    {
        try
        {
            this.connection = DriverManager.getConnection( "jdbc:h2:mem:mydb" );

            // Calling this in the constructor should be save
            // because the insert statement says "IF NOT EXISTS"
            this.createTable();
        }
        catch ( SQLException e )
        {
            e.printStackTrace();

            throw new DataAccessException( "Error while creating CustomerDao - could not open DB connection" );
        }
    }

    public boolean insertCustomer( Customer customer )
    {
        boolean success = true;

        try
        {
            PreparedStatement preparedStatement = this.connection.prepareStatement( INSERT_CUSTOMER );
            preparedStatement.setString( 1, customer.getFirstname() );
            preparedStatement.setString( 2, customer.getLastname() );
            preparedStatement.setString( 3, customer.getPreferences() );

            preparedStatement.execute();
        }
        catch ( SQLException e )
        {
            e.printStackTrace();

            success = false;
        }

        return success;
    }

    private final boolean createTable()
    {
        boolean success = true;

        try
        {
            Statement statement = this.connection.createStatement();

            statement.execute( CREATE_CUSTOMER_TABLE );
        }
        catch ( SQLException e )
        {
            e.printStackTrace();

            success = false;
        }

        return success;
    }

    public List<Customer> getAllCustomers()
    {
        List<Customer> customers = new ArrayList<Customer>();

        try
        {
            Statement statement = this.connection.createStatement();

            ResultSet dbCustomers = statement.executeQuery( READ_ALL_CUSTOMERS );

            while ( dbCustomers.next() )
            {
                Customer customer = new Customer();

                customer.setId( dbCustomers.getLong( COLUMN_ID ) );
                customer.setFirstname( dbCustomers.getString( COLUMN_FIRST_NAME ) );
                customer.setLastname( dbCustomers.getString( COLUMN_LAST_NAME ) );
                customer.setPreferences( dbCustomers.getString( COLUMN_PREFERENCES ) );

                customers.add( customer );
            }
        }
        catch ( SQLException e )
        {
            e.printStackTrace();

            return null;
        }

        return customers;
    }

}
