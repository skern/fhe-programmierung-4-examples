package de.fhe.ai.servertemplate.model;

import java.util.ArrayList;
import java.util.List;

public class Addressbook
{
    private static List<Person> personList = new ArrayList<Person>(  );

    static
    {
        personList.add( new Person( "Max", "Mustermann" ) );
        personList.add( new Person( "Susi", "Sorglos" ) );
    }

    public static List<Person> getPersonList()
    {
        return personList;
    }

    public static void setPersonList( List<Person> _personList )
    {
        personList = _personList;
    }
}
