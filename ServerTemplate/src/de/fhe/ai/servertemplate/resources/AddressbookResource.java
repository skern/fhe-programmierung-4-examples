package de.fhe.ai.servertemplate.resources;

import de.fhe.ai.servertemplate.model.Addressbook;
import de.fhe.ai.servertemplate.model.Person;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Path( "/addressbook" )
public class AddressbookResource
{
    @GET
    @Produces( MediaType.APPLICATION_JSON )
    public List<Person> getAddressbook()
    {
        return Addressbook.getPersonList();
    }

    @DELETE
    public Response deleteAddressbook()
    {
        Addressbook.setPersonList( new ArrayList<Person>() );

        return Response.noContent().build();
    }

    @POST
    @Consumes ( MediaType.APPLICATION_JSON )
    public Response createPerson( Person newPerson )
    {
        Addressbook.getPersonList().add( newPerson );

        try
        {
            return Response.created( new URI( "/addressbook/" + Addressbook.getPersonList().indexOf( newPerson ) ) ).build();
        }
        catch ( URISyntaxException e )
        {
            return Response.status( Response.Status.INTERNAL_SERVER_ERROR ).build();
        }
    }

    /*
        Return Person Sub-Resource

        The request will afterwards be handled by the Sub-Resource
     */
    @Path( "/{personId}" )
    public PersonResource getPerson( @PathParam( "personId" ) int personId )
    {
        return new PersonResource( personId );
    }
}
