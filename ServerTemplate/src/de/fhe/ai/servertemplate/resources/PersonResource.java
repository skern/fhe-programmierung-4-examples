package de.fhe.ai.servertemplate.resources;

import de.fhe.ai.servertemplate.model.Addressbook;
import de.fhe.ai.servertemplate.model.Person;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class PersonResource
{
    private int personId;

    public PersonResource( int personId )
    {
        this.personId = personId;
    }

    @GET
    @Produces( MediaType.APPLICATION_JSON )
    public Person getPerson( )
    {
        this.isPersonIdValid( this.personId );

        return Addressbook.getPersonList().get( this.personId );
    }

    @DELETE
    public Response deletePerson()
    {
        this.isPersonIdValid( this.personId );

        Addressbook.getPersonList().remove( this.personId );

        return Response.noContent().build();
    }

    private void isPersonIdValid( int personId )
    {
        if( personId < 0 || personId >= Addressbook.getPersonList().size() )
        {
            throw new IllegalArgumentException( "Unknown Person ID" );
        }
    }
}
