package de.fhe.ai.servertemplate.core;

import de.fhe.ai.servertemplate.model.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class IlleagalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException>
{
    @Override
    public Response toResponse( IllegalArgumentException e )
    {
        ErrorMessage msg = new ErrorMessage( e.getMessage() );

        return Response.status( Response.Status.NOT_FOUND )
                       .entity( msg )
                       .build();
    }
}
