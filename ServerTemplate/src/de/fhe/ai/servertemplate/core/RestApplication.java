package de.fhe.ai.servertemplate.core;

import de.fhe.ai.servertemplate.resources.AddressbookResource;
import de.fhe.ai.servertemplate.resources.PersonResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath( "api" )
public class RestApplication extends Application
{
    public RestApplication( ) {}

    /*
        This method needs to be overridden if a Web Application
        wants to provide a REST API.

        Any Java class that offers/implements a REST interface
        as well as all classes marked with <code>@Provider</code>
        must be added to the returned Set to be available.
     */
    @Override
    public Set<Class<?>> getClasses( )
    {
        final Set<Class<?>> returnValue = new HashSet<Class<?>>( );

        returnValue.add( AddressbookResource.class );
        returnValue.add( PersonResource.class );
        returnValue.add( IlleagalArgumentExceptionMapper.class );

        return returnValue;
    }
}
