# Einleitung

Das Beispielprojekt stellt ein vollständige Serveranwendung mit einer REST Schnittstelle dar. Thematischer Hintergrund ist das aus der Vorlesung bekannte Beispiel eines Adressbuchs. Die Anwendung ermöglich das Abfragen aller bzw. einzelner Personen aus dem Adressbuch sowie das Anlegen und Löschen von Personen.

# Pakete und Klassen

Das Projekt gliedert sich in mehrere Java-Packages.

## Package **core**

Dieses Paket enthält die Basisklassen des Projektes. Zum einen die Klasse **RestApplication**, welche die Serveranwendung konfiguriert sowie die Klasse **IlleagalArgumentExceptionMapper**, welche für die Erzeugung einer Serverantwort im Falle einer aufgetretenen **IlleagalArgumentException** generiert.

### Klasse **RestApplication**
Diese Klasse konfiguriert die Anwendung bzw. welche Klassen die REST Schnittstelle bereitstellen. Dazu wird in der überschriebenen Methode **getClasses()** eine Liste von Klassen zurückgegeben, welche innerhalb der REST Schnittstelle zum Einsatz kommen. Sprich, wenn bspw. neue Resourcen-Klassen oder Klassen für die Behandlung von Fehler (Stichwort _ExceptionMapper_) zum Projekt hinzukommen, müssen diese in jener Methode ergänzt werden!

### Klasse **IlleagalArgumentExceptionMapper**
Dies Klasse dient der Verarbeitung von **IlleagalArgumentException**, welche _irgendwo_ innerhalb der Anwendung geworfen werden. Die Klasse wird automatisch vom Java Web Application Framework _aktiviert_, sobald eine entsprechende Exception aufgetreten ist. Eine eigene Behandlung der Exception via _try-catch_ ist nicht nötig. Für jeden Exception-Typ, welcher innerhalb Ihrer Anwendung auftritt, sollte ein eigener _ExceptionMapper_ implementiert werden. Dieser muss dann das Interface **ExceptionMapper<IhreExceptionKlasse>** implementieren.
    
## Package **model**
Das Paket **model** enthält die Modellklassen der Anwendung. Neben **Addressbook** und **Person**, welche die eigentlichen *Nutzdaten* der Anwendung darstellen, gibt es noch die Klasse **ErrorMessage**. Diese wird zur Beschreibung von Fehlermeldungen verwendet, welche an den Client gesendet werden.

Die Modellklassen werden durch das Server Framework automatisch in JSON umgewandelt, sobald sie an den Client gesendet werden sollen. Eine eigene Behandlung ist nicht nötig. Ebenso erfolgt eine automatische Umwandlung von JSON in ein entsprechendes Java-Objekt, wenn entsprechende Daten von Client gesendet werden. Voraussetzung dafür ist, dass jede Modellklasse einen Argument-losen Konstruktor sowie Get- & Set-Methoden für die Membervariablen besitzt.
    
## Package **resources**
Das Paket **resources** enthält die alle Klassen, welche die REST Schnittstelle der Anwendung implementieren. Es gibt jeweils eine Klasse für die Verarbeitung von Anfragen an das gesamte Adressbuch (**AddressbookResource**) sowie an einzelne Personen (**PersonResource**). Die Resourcenklassen besitzen Annotationen, welche beschreiben, auf welche URL bzw. welchen URL Pfad die einzelnen Methoden gemappt werden sollten sowie die HTTP Methode (GET, POST, PUT, DELETE), welche von den Methoden verarbeitet werden. Im folgenden Kapitel werden die Zusammenhänge dargestellt.

# Resourcen & URLs

Basis-URL der Anwendung: 
http://(servername oder IP):8080/api oder http://(servername oder IP):8080/appname/api

Abhängig davon, wie Sie die Installation der Anwendung in einem Application Server konfiguriert haben, enthält die URL einen Applikationsnamen (appname) oder nicht.

Alle nachfolgenden URL sind relativ zur Basis-URL zu betrachten. Sprich zur relativen URL /addressbook gehört die vollständige URL http://(servername oder IP):8080/api/addressbook bzw. http://(servername oder IP):8080/appname/api/addressbook.

## URLs/Anfragen für die Bearbeitung des Adressbuchs

### Abrufen des Adressbuchs

**Server Anfrage**

* HTTP Methode: GET
* Relative URL: /addressbook 

-----

**Server Antwort**
 
* Statuscode:  200
* JSON Antwort:


    [
        {
            "firstName":"Max",
            "lastName":"Mustermann"
        },
        {
            "firstName":"Susi",
            "lastName":"Sorglos"
        }
    ]



### Löschen des gesamten Adressbuchs

**Server Anfrage**

* HTTP Methode: DELETE
* Relative URL: /addressbook

-----

**Server Antwort**

* Statuscode: 204
* Json Antwort: kein Inhalt


### Anlegen einer neuen Person

**Server Anfrage**

* HTTP Methode: POST
* Relative URL: /addressbook
* _Content-Type_ Header: application/json
* Anfrage Body/JSON:


    {
        "firstName":"Karl",
        "lastName":"Klammer"
    }

-----

**Server Antwort**

* Statuscode: 201
* Header Parameter _Location_: http://localhost:8080/addressbook/2

   
## URLs/Anfragen für die Bearbeitung von Personen

###Abruf einer einzelnen Person

**Server Anfrage**

* HTTP Methode: GET
* Relative URL: /addressbook/personId  - z.B. /addressbook/1

Die _personId_ entspricht dem Index der Person innerhalb der Liste, in welcher alle Personen verwaltet werden. Ideal ist hier natürlich einen Datenbank-ID o.ä.

-----

**Server Antwort**

* Statuscode: 200
* JSON Antwort :  


    {
        "firstName":"Max",
        "lastName":"Mustermann"
    } 

###Abruf einer Person mit ungültiger ID

In diesem Fall wird eine _IlleagalArgumentException_ geworfen und der ExceptionMapper greift ein und erzeugt die Serverantwort.

**Server Anfrage**

* HTTP Methode: GET
* Relative URL: z.B. /addressbook/1000

-----

**Server Antwort**

* Antwort Statuscode: 404
* JSON Antwort:  


    {
        "message":"Unknown Person ID"
    }


###Löschen einer Person 

**Server Anfrage**

* HTTP Methode: DELETE
* Relative URL: /addressbook/1

-----

**Server Antwort**

* Statuscode: 204
* Json Antwort: kein Inhalt