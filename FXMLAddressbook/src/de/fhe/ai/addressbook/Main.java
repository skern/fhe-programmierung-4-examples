package de.fhe.ai.addressbook;

import com.thoughtworks.xstream.XStream;
import de.fhe.ai.addressbook.control.MainLayoutController;
import de.fhe.ai.addressbook.control.PersonEditDialogController;
import de.fhe.ai.addressbook.control.PersonOverviewController;
import de.fhe.ai.addressbook.model.Person;
import de.fhe.ai.util.FileUtil;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.prefs.Preferences;

public class Main extends Application
{
    private ObservableList<Person> persons = FXCollections.observableArrayList();

    private Stage primaryStage;
    private BorderPane rootLayout;

    /**
     * Constructor
     * Automatically called when launching the app.
     * Does just add some test persons to our person list.
     */
    public Main()
    {
        this.persons.addAll( new Person( "Max", "Mustermann" ) );
        this.persons.addAll( new Person( "Susi", "Sorglos" ) );
    }

    @Override
    public void start(Stage primaryStage)
    {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle( "AddressApp" );
        this.primaryStage.getIcons().add( new Image( "file:resources/images/Icon_Addressbook.png" ) );

        try
        {
            // Load the root layout from the fxml file
            FXMLLoader loader = new FXMLLoader( getClass().getResource("view/MainLayout.fxml") );
            rootLayout = (BorderPane) loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

            // Give the controller access to the main app
            MainLayoutController controller = loader.getController();
            controller.setMainApp( this );

            showPersonOverview();

            // Try to load last opened person file
            File file = getPersonFilePath();
            if ( file != null )
            {
                loadPersonDataFromFile( file );
            }

        }
        catch (IOException e)
        {
            // Exception gets thrown if the fxml file could not be loaded
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage()
    {
        return primaryStage;
    }

    /**
     * Returns the list of persons maintained by the app     *
     * @return
     */
    public ObservableList<Person> getPersons()
    {
        return persons;
    }

    /**
     * Shows the person overview scene.
     */
    public void showPersonOverview()
    {
        try
        {
            // Load the fxml file and set into the center of the main layout
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/PersonOverview.fxml"));
            AnchorPane overviewPage = (AnchorPane) loader.load();
            rootLayout.setCenter(overviewPage);

            // Give the controller access to the main app
            PersonOverviewController controller = loader.getController();
            controller.setMainApp( this );

        }
        catch (IOException e)
        {
            // Exception gets thrown if the fxml file could not be loaded
            e.printStackTrace();
        }
    }

    public boolean showPersonEditDialog(Person person)
    {
        try
        {
            // Load the fxml file and create a new stage for the popup
            FXMLLoader loader = new FXMLLoader( getClass().getResource("view/PersonEditDialog.fxml") );
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Kontakt bearbeiten");
            dialogStage.initModality( Modality.WINDOW_MODAL );
            dialogStage.initOwner( primaryStage );
            Scene scene = new Scene( page );
            dialogStage.setScene( scene );

            // Set the person into the controller
            PersonEditDialogController controller = loader.getController();
            controller.setDialogStage( dialogStage );
            controller.setPerson( person );

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        }
        catch (IOException e)
        {
            // Exception gets thrown if the fxml file could not be loaded
            e.printStackTrace();
            return false;
        }
    }

    /*
        Data <-> File Handling
     */

    /**
     * Loads person data from the specified file. The current person data will
     * be replaced.
     *
     * @param file
     */
    public void loadPersonDataFromFile(File file)
    {
        XStream xstream = new XStream();
        xstream.alias("person", Person.class);

        try
        {
            String xml = FileUtil.readFile(file);

            ArrayList<Person> personList =
                    (ArrayList<Person>)xstream.fromXML( xml );

            persons.clear();
            persons.addAll(personList);

            setPersonFilePath(file);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Saves the current person data to the specified file.
     *
     * @param file
     */
    public void savePersonDataToFile(File file)
    {
        XStream xstream = new XStream();
        xstream.alias("person", Person.class);

        // Convert ObservableList to a normal ArrayList
        ArrayList<Person> personList = new ArrayList<Person>( persons );

        String xml = xstream.toXML(personList);
        try
        {
            FileUtil.saveFile(xml, file);

            setPersonFilePath(file);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    /*
        Preference Handling
     */

    /**
     * Returns the person file preference, i.e. the file that was last opened.
     * The preference is read from the OS specific registry. If no such
     * preference can be found, null is returned.
     *
     * @return
     */
    public File getPersonFilePath()
    {
        Preferences prefs = Preferences.userNodeForPackage( Main.class );
        String filePath = prefs.get("filePath", null);
        if (filePath != null)
        {
            return new File(filePath);
        }
        else
        {
            return null;
        }
    }

    /**
     * Sets the file path of the currently loaded file.
     * The path is persisted in the OS specific registry.
     *
     * @param file the file or null to remove the path
     */
    public void setPersonFilePath( File file )
    {
        Preferences prefs = Preferences.userNodeForPackage( Main.class );
        if (file != null)
        {
            prefs.put("filePath", file.getPath());

            // Update the stage title
            primaryStage.setTitle("AddressApp - " + file.getName());
        }
        else
        {
            prefs.remove("filePath");

            // Update the stage title
            primaryStage.setTitle("AddressApp");
        }
    }


    public static void main(String[] args)
    {
        launch(args);
    }
}
