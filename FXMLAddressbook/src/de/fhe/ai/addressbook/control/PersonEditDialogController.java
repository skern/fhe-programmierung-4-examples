package de.fhe.ai.addressbook.control;

import de.fhe.ai.addressbook.model.Person;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class PersonEditDialogController
{
    @FXML
    private TextField firstnameField;
    @FXML
    private TextField lastnameField;
    @FXML
    private TextField phoneField;
    @FXML
    private TextField mailField;


    private Stage dialogStage;
    private Person person;
    private boolean okClicked = false;

    /**
     * Initializes the controller class.
     * Automatically called after the fxml
     * file has been loaded.
     */
    @FXML
    private void initialize() {}

    /**
     * Sets the stage of this dialog.
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage)
    {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     *
     * @param person
     */
    public void setPerson(Person person)
    {
        this.person = person;

        firstnameField.setText( person.getFirstname() );
        lastnameField.setText( person.getLastname() );
        phoneField.setText( person.getPhone() );
        mailField.setText( person.getMail());
    }

    /**
     * Returns true if the user clicked OK,
     * false otherwise.
     * @return
     */
    public boolean isOkClicked()
    {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk()
    {
        if ( isInputValid() )
        {
            person.setFirstname( firstnameField.getText() );
            person.setLastname( lastnameField.getText() );
            person.setPhone( phoneField.getText() );
            person.setMail( mailField.getText() );

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel()
    {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid()
    {
        // Check input values here

        return true;
    }
}