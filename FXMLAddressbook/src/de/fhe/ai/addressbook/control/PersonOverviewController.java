package de.fhe.ai.addressbook.control;

import de.fhe.ai.addressbook.Main;
import de.fhe.ai.addressbook.model.Person;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class PersonOverviewController
{
    @FXML
    private TableView<Person> personTable;
    @FXML
    private TableColumn<Person, String> firstnameColumn;
    @FXML
    private TableColumn<Person, String> lastnameColumn;

    @FXML
    private Label firstnameLabel;
    @FXML
    private Label lastnameLabel;
    @FXML
    private Label phoneLabel;
    @FXML
    private Label mailLabel;

    private Main mainApp;


    public PersonOverviewController() {}


    private void showPersonDetails( Person person )
    {
        if( person != null )
        {
            this.firstnameLabel.setText(person.getFirstname());
            this.lastnameLabel.setText( person.getLastname() );
            this.phoneLabel.setText( person.getPhone() );
            this.mailLabel.setText( person.getMail() );
        }
        else
        {
            this.firstnameLabel.setText( "" );
            this.lastnameLabel.setText( "" );
            this.phoneLabel.setText( "" );
            this.mailLabel.setText( "" );
        }
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDeletePerson()
    {
        int selectedIndex = this.personTable.getSelectionModel().getSelectedIndex();

        if( selectedIndex >= 0 )
        {
            this.personTable.getItems().remove( selectedIndex );
        }
    }


    /**
     * Initializes the controller class.
     * Automatically called after the fxml file has been loaded.
     */
    @FXML
    private void initialize()
    {
        // Init Table Columns
        firstnameColumn.setCellValueFactory( new PropertyValueFactory<Person, String>("firstname") );
        lastnameColumn.setCellValueFactory( new PropertyValueFactory<Person, String>("lastname") );

        // Auto resize columns
        personTable.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );

        // Clear Person Details
        showPersonDetails(null);

        // Listen for selection changes
        personTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Person>() {

            @Override
            public void changed(ObservableValue<? extends Person> observable,
                                Person oldValue, Person newValue) {
                showPersonDetails( newValue );
            }
        });
    }

    /**
     * Called when the user clicks the new button.
     * Opens a dialog to edit details for a new person.
     */
    @FXML
    private void handleNewPerson()
    {
        Person tempPerson = new Person();
        boolean okClicked = mainApp.showPersonEditDialog( tempPerson );
        if ( okClicked )
        {
            mainApp.getPersons().add( tempPerson );
        }
    }

    /**
     * Called when the user clicks the edit button.
     * Opens a dialog to edit details for the selected person.
     */
    @FXML
    private void handleEditPerson()
    {
        Person selectedPerson = personTable.getSelectionModel().getSelectedItem();

        if ( selectedPerson != null )
        {
            boolean okClicked = mainApp.showPersonEditDialog(selectedPerson);
            if ( okClicked )
            {
                refreshPersonTable();
                showPersonDetails(selectedPerson);
            }

        }
    }

    /**
     * Need to refresh the table after editing an existing
     * person.
     * Could be accomplished by using property bindings,
     * however this would have made this small example
     * overly complex
     */
    private void refreshPersonTable()
    {
        int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
        personTable.setItems( null );
        personTable.layout();
        personTable.setItems( mainApp.getPersons() );
        personTable.getSelectionModel().select(selectedIndex);
    }


    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        personTable.setItems( mainApp.getPersons() );
    }
}
