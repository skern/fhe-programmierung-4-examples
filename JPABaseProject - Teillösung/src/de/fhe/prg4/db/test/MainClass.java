package de.fhe.prg4.db.test;

import de.fhe.prg4.db.core.DataController;
import de.fhe.prg4.db.core.ICustomerDao;
import de.fhe.prg4.db.core.IEmployeeDao;
import de.fhe.prg4.db.core.IGenericDao;
import de.fhe.prg4.db.model.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MainClass
{

    // Member to hold test data
    private static List<Division> divisions;
    private static List<Project> projects;
    private static List<Person> persons;

    public static void main(String[] args)
    {
        // Create Demo Data
        createTestData();

        // Get a reference to our DataController singleton
        DataController dc = DataController.getInstance();

        // Now create several Dao - the DataController Singleton will take care of creating
        // a single EntityManagerFactory as well as an EntityManager for each Dao.

        // We use the IGenericDao interface and its sub-interfaces (ICustomerDao & IEmployeeDao)
        // as reference type to hide the concrete implementation class of our Dao.

        // One generic Dao for all Person objects (this includes Customer, Employee and Executive)
        IGenericDao<Person> personDao = dc.getPersonDao();

        // Two special Dao for specific classes which provide additional methods for easier
        // handling
        ICustomerDao customerDao = dc.getCustomerDao();
        IEmployeeDao employeeDao = dc.getEmployeeDao();

        // Iterate the persons list - depending on the concrete class at the current position,
        // we use the corresponding Dao to save the object/create a database entry
        for( Person p : persons )
        {
            personDao.create( p );
        }

        // Let's use some functions of our specialized Dao
        Collection<Customer> schmidtCustomers = customerDao.findCustomersByLastname( "Schmidt" );
        for( Customer c : schmidtCustomers )
        {
            System.out.println( c );
        }

        // Find Employees with a specific salary, decrease it and save
        Collection<Employee> highSalaryEmployees = employeeDao.findEmployeesWithSalaryAbove( 2000.0 );
        for( Employee e : highSalaryEmployees )
        {
            e.setSalary( 1500.0 );
            employeeDao.update( e );
        }

        // Now let's check of all Employees have a maximum salary of 2000
        Collection<Employee> allEmployees = employeeDao.findAll();
        for( Employee e : allEmployees )
        {
            System.out.println( e );
        }

    }

    /*
        Helper Method to create some objects
     */
    private static void createTestData()
    {
        persons = new ArrayList<Person>();

        persons.add( new Customer( "Mustermann", "Max", "Farbe Blau" ) );
        persons.add( new Customer( "Müller", "Gabi", "Farbe Rosa" ) );
        persons.add( new Customer( "Schmidt", "Heinz", "Farbe Grün" ) );

        persons.add( new Employee( "Brown", "Joe", 2000.0) );
        persons.add( new Employee( "White", "Dave", 2200.0) );
        persons.add( new Employee( "Black", "Micheal", 1700.0) );

        persons.add( new Executive( "Gates", "Bill", 2000.0, 200.0) );
        persons.add( new Executive( "Schmidt", "Eric", 2200.0, 100.0) );
        persons.add( new Executive( "Zuckerberg", "Mark", 1700.0, 50.0) );


        divisions = new ArrayList<Division>();

        Division d = new Division( "Abteilung 1", (Executive)persons.get( 6 ) );
        d.getMembers().add( (Employee)persons.get( 3 ) );
        d.getMembers().add( (Employee)persons.get( 4 ) );
        divisions.add( d );

    }


}
