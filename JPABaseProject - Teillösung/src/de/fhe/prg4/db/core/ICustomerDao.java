package de.fhe.prg4.db.core;

import de.fhe.prg4.db.model.Customer;

import java.util.Collection;

/*
    Interface for our specific Customer Dao

    Like its super interface, it completely hides all the
    JPA/Database related stuff.

    Furthermore, it defines an additional method for easier
    handling of Customers - thus, such a method should not
    appear in the super interface as its functionality only
    applies to Customer instances.

 */
public interface ICustomerDao extends IGenericDao<Customer>
{
    public Collection<Customer> findCustomersByLastname( String lastname );
}
