package de.fhe.prg4.db.core;

import de.fhe.prg4.db.model.Employee;

import java.util.Collection;

/*
    Interface for our specific Employee Dao

    Like its super interface, it completely hides all the
    JPA/Database related stuff.

    Furthermore, it defines an additional method for easier
    handling of Employees - thus, such a method should not
    appear in the super interface as its functionality only
    applies to Employee instances.

 */
public interface IEmployeeDao extends IGenericDao<Employee>
{
    public Collection<Employee> findEmployeesWithSalaryAbove( double minSalary );
}
