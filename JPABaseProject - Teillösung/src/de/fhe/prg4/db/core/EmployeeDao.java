package de.fhe.prg4.db.core;

import de.fhe.prg4.db.model.Employee;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collection;

class EmployeeDao extends JPAGenericDao<Employee> implements IEmployeeDao
{
    public EmployeeDao( EntityManager em )
    {
        super( Employee.class, em );
    }

    @Override
    public Collection<Employee> findEmployeesWithSalaryAbove( double minSalary )
    {
        Query query = getEntityManager().createQuery(
                "SELECT e FROM Employee e WHERE e.salary > :minSalary" );
        query.setParameter( "minSalary", minSalary );

        return (Collection<Employee>) query.getResultList();
    }
}
