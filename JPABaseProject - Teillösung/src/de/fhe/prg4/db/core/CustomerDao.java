package de.fhe.prg4.db.core;

import de.fhe.prg4.db.model.Customer;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collection;

class CustomerDao extends JPAGenericDao<Customer> implements ICustomerDao
{
    public CustomerDao( EntityManager em )
    {
        super( Customer.class, em );
    }

    @Override
    public Collection<Customer> findCustomersByLastname( String lastname )
    {
        Query query = getEntityManager().createQuery(
                "SELECT c FROM Customer as c WHERE c.lastname = :lastname" );
        query.setParameter( "lastname", lastname );

        return (Collection<Customer>) query.getResultList();
    }
}
