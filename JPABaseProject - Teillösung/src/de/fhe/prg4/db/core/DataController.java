package de.fhe.prg4.db.core;

import de.fhe.prg4.db.model.Executive;
import de.fhe.prg4.db.model.Person;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/*
    This class is used to create Dao instances and to hide all
    the database related stuff. Therefore, it can be considered
    as a Facade to the database module/package.

    It is implemented as a singleton and holds a reference to the
    sole instance of our EntityManagerFactory that manages our
    persistence context.
 */
public class DataController
{
    private static final String PERSISTENCE_UNIT_NAME = "company-pu";

    private EntityManagerFactory entityManagerFactory;

    /*
        Singleton definition
     */
    private static DataController instance;

    public static DataController getInstance()
    {
        if( instance == null )
            instance = new DataController();

        return instance;
    }

    /*
        Private Constructor
     */

    private DataController()
    {
        this.entityManagerFactory = Persistence.createEntityManagerFactory( PERSISTENCE_UNIT_NAME );
    }

    /*
        Dao Getter
     */

    public ICustomerDao getCustomerDao()
    {
        return new CustomerDao( this.entityManagerFactory.createEntityManager() );
    }

    public IEmployeeDao getEmployeeDao()
    {
        return new EmployeeDao( this.entityManagerFactory.createEntityManager() );
    }

    public IGenericDao<Executive> getExecutiveDao()
    {
        return new JPAGenericDao<Executive>( Executive.class,
                this.entityManagerFactory.createEntityManager() );
    }

    public IGenericDao<Person> getPersonDao()
    {
        return new JPAGenericDao<Person>( Person.class,
                this.entityManagerFactory.createEntityManager() );
    }

}
