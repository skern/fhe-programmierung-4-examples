package de.fhe.prg4.db.model;

import de.fhe.prg4.db.core.AbstractDatabaseEntity;

import javax.persistence.*;

@Entity
public abstract class Person extends AbstractDatabaseEntity
{
    private String lastname;
    private String firstname;

    /*
        Simpler Konstruktor ohne Parameter
        Verwendet den zweiten Konstrutor dieser Klasse
     */
    public Person()
    {
        this( "unknown", "unknown" );
    }

    /*
        "Haupt-Konstruktor" der Klasse
        Initialisiert Vorname und Nachname mit den übergebenen Parametern
     */
    public Person( String lastname, String firstname )
    {
        this.lastname = lastname;
        this.firstname = firstname;
    }

    /*
        Getter & Setter
     */

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }


    /*
        Helper Methods
     */

    @Override
    public String toString()
    {
        return this.getFirstname() + " " + this.getLastname();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!firstname.equals(person.firstname)) return false;
        if (!lastname.equals(person.lastname)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = lastname.hashCode();
        result = 31 * result + firstname.hashCode();

        return result;
    }
}
