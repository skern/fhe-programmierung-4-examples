#Hinweise
***

Das Repository enthält die folgenden Beispielprojekte:

###JDBC Übungsprojekt
Vorlage für die erste Übung - Datenbankanbindung mit JDBC

###JDBC Übungsprojekt - Teillösung
Aufbauend auf dem 1. Projekt eine Teillösung der Übung

###JPABaseProject
Vorlage für die dritte Übung - Datenbankanbindung mit JPA

###JPABaseProject - Teillösung
Aufbauend auf dem 3. Projekt stellt dieses Projekt eine Beispiellösung für die Nutzung von Daos, EntityManagerFactory und EntityManagern dar.

###ServerTemplate
Enthält eine beispielhafte Java Serveranwendung, welche eine REST API zur Verwaltung eines Adressbuchs implementiert. Bitte einen Blick in die README (verfügbar als md, html & pdf) des Projektes werfen. 