package de.fhe.prg4.db.test;

import de.fhe.prg4.db.core.GenericDao;
import de.fhe.prg4.db.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class MainClass
{
    private static final String PERSISTENCE_UNIT_NAME = "company-pu";
    private static EntityManagerFactory factory;

    // Member to hold test data
    private static List<Division> divisions;
    private static List<Project> projects;
    private static List<Person> persons;

    public static void main(String[] args)
    {
        // Create Demo Data
        createTestData();

        // Create a Database Connection / Init Persistence Unit
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();

        // Save the Data
        em.getTransaction().begin();

        for( Person p : persons )
            em.persist( p );

        for( Division d : divisions )
            em.persist( d );

        em.getTransaction().commit();

        // Check if all Persons got a valid ID
        for ( Person p : persons )
            System.out.println( p.getId() + ": " + p.getLastname() );

    }

    /*
        Helper Method to create some objects
     */
    private static void createTestData()
    {
        persons = new ArrayList<Person>();

        persons.add( new Customer( "Mustermann", "Max", "Farbe Blau" ) );
        persons.add( new Customer( "Müller", "Gabi", "Farbe Rosa" ) );
        persons.add( new Customer( "Schmidt", "Heinz", "Farbe Grün" ) );

        persons.add( new Employee( "Brown", "Joe", 2000.0) );
        persons.add( new Employee( "White", "Dave", 2200.0) );
        persons.add( new Employee( "Black", "Micheal", 1700.0) );

        persons.add( new Executive( "Gates", "Bill", 2000.0, 200.0) );
        persons.add( new Executive( "Schmidt", "Eric", 2200.0, 100.0) );
        persons.add( new Executive( "Zuckerberg", "Mark", 1700.0, 50.0) );


        divisions = new ArrayList<Division>();

        Division d = new Division( "Abteilung 1", (Executive)persons.get( 6 ) );
        d.getMembers().add( (Employee)persons.get( 3 ) );
        d.getMembers().add( (Employee)persons.get( 4 ) );
        divisions.add( d );

    }


}
