package de.fhe.prg4.db.model;

import de.fhe.prg4.db.core.AbstractDatabaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Division extends AbstractDatabaseEntity
{
    private String name;

    @OneToOne
    private Executive lead;

    @OneToMany( mappedBy = "division" )
    private List<Employee> members;


    public Division() {}

    public Division(String name, Executive lead) {
        this.name = name;
        this.lead = lead;
        this.members = new ArrayList<Employee>();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Executive getLead()
    {
        return lead;
    }

    public void setLead(Executive lead)
    {
        this.lead = lead;
    }

    public List<Employee> getMembers()
    {
        return members;
    }

    public void setMembers(List<Employee> members)
    {
        this.members = members;
    }
}
