package de.fhe.prg4.db.model;

import de.fhe.prg4.db.core.AbstractDatabaseEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Project extends AbstractDatabaseEntity
{
    private String name;

    @ManyToMany
    private List<Employee> team;

    public Project() {}

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Employee> getTeam()
    {
        return team;
    }

    public void setTeam(List<Employee> team)
    {
        this.team = team;
    }
}
