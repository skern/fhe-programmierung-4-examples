package de.fhe.prg4.db.core;

import de.fhe.prg4.db.model.Person;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class GenericDao<T extends AbstractDatabaseEntity, ID extends Serializable>
{
    private final Class<T> persistentClass;
    private EntityManager entityManager;

    public GenericDao( Class<T> type, EntityManager em )
    {
        this.persistentClass = type;
        this.entityManager = em;
    }

    public T findById( final ID id )
    {
        final T result = getEntityManager().find( persistentClass, id );
        return result;
    }

    public Collection<T> findAll()
    {
        Query query = getEntityManager().createQuery(
                "SELECT e FROM " + getEntityClass().getCanonicalName() + " e" );
        return (Collection<T>) query.getResultList();
    }

    public void create( T entity )
    {
        getEntityManager().getTransaction().begin();
        getEntityManager().persist( entity );
        getEntityManager().getTransaction().commit();

    }

    public T update( T entity )
    {
        getEntityManager().getTransaction().begin();
        final T savedEntity = getEntityManager().merge( entity );
        getEntityManager().getTransaction().commit();

        return savedEntity;
    }

    public void delete( ID id )
    {
        T entity = this.findById( id );
        this.delete( entity );
    }

    public void delete( T entity )
    {
        getEntityManager().getTransaction().begin();
        getEntityManager().remove( entity );
        getEntityManager().getTransaction().commit();
    }

    public void delete( List<T> entries )
    {
        getEntityManager().getTransaction().begin();

        for( T entry : entries )
        {
            getEntityManager().remove( entry );
        }

        getEntityManager().getTransaction().commit();
    }

    /*
        Getter & Setter
     */

    public Class<T> getEntityClass()
    {
        return persistentClass;
    }

    public void setEntityManager( final EntityManager entityManager )
    {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager()
    {
        return entityManager;
    }

}
