package de.fhe.prg4.db.test;

import de.fhe.prg4.db.model.Customer;
import de.fhe.prg4.db.model.Employee;
import de.fhe.prg4.db.model.Executive;

import java.util.ArrayList;
import java.util.List;

public class MainClass
{
    public static void main(String[] args)
    {
        List<Customer> customers = new ArrayList<Customer>();

        customers.add( new Customer( "Mustermann", "Max", "Farbe Blau" ) );
        customers.add( new Customer( "Müller", "Gabi", "Farbe Rosa" ) );
        customers.add( new Customer( "Schmidt", "Heinz", "Farbe Grün" ) );

        List<Employee> employees = new ArrayList<Employee>();

        employees.add( new Employee( "Brown", "Joe", 2000.0) );
        employees.add( new Employee( "White", "Dave", 2200.0) );
        employees.add( new Employee( "Black", "Micheal", 1700.0) );

        employees.add( new Executive( "Gates", "Bill", 2000.0, 200.0) );
        employees.add( new Executive( "Schmidt", "Eric", 2200.0, 100.0) );
        employees.add( new Executive( "Zuckerberg", "Mark", 1700.0, 50.0) );
    }

}
