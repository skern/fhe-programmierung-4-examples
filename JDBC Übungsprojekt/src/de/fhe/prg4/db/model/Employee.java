package de.fhe.prg4.db.model;

public class Employee extends Person
{
    private double salary;

    /*
        Simpler Konstruktor
     */
    public Employee()
    {
        super();
        this.salary = 0.0;
    }

    /*
        "Haupt-Konstruktor"
     */
    public Employee( String lastname, String firstname, double salary )
    {
        super( lastname, firstname );
        this.salary = salary;
    }

    /*
        Getter & Setter
     */

    public double getSalary()
    {
        return salary;
    }

    public void setSalary(double salary)
    {
        this.salary = salary;
    }

    /*
        Helper
     */

    @Override
    public String toString() {
        return "Employee: " + super.toString() +
               "\n\tSalary: " + salary;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (!super.equals(o)) return false;

        Employee employee = (Employee) o;

        if (Double.compare(employee.salary, salary) != 0) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(salary);
        result = 31 * result + (int) (temp ^ (temp >>> 32));

        return result;
    }
}
