package de.fhe.prg4.db.model;

public class Executive extends Employee
{
    private double bonus;

    /*
        Simpler Konstruktor
     */
    public Executive()
    {
        super();
        this.bonus = 0.0;
    }

    /*
        "Haupt-Konstruktor"
     */
    public Executive( String lastname, String firstname, double salary, double bonus )
    {
        super( lastname, firstname, salary );
        this.bonus = bonus;
    }

    /*
        Getter & Setter
     */

    public double getBonus()
    {
        return bonus;
    }

    public void setBonus(double bonus)
    {
        this.bonus = bonus;
    }

    /*
        Helper
     */

    @Override
    public String toString() {
        return super.toString() +
               "\n\t Executive bonus: " + this.bonus;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Executive executive = (Executive) o;

        if (Double.compare(executive.bonus, bonus) != 0) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(bonus);
        result = 31 * result + (int) (temp ^ (temp >>> 32));

        return result;
    }
}
