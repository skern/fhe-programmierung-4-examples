package de.fhe.ai.addressbook.server.beans;

import de.fhe.ai.addressbook.server.database.ContactDao;
import de.fhe.ai.addressbook.server.interceptor.Logging;
import de.fhe.ai.addressbook.server.model.Contact;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@Logging
public class AddressbookService
{
    @Inject
    private ContactDao contactDao;


    public List<Contact> getAllContacts()
    {
        return this.contactDao.getAllContacts();
    }
}
