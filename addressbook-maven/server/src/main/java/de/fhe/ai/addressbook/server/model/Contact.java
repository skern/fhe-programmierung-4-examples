package de.fhe.ai.addressbook.server.model;

import javax.persistence.*;

@Entity
@Table( name = "CONTACTS" )
public class Contact extends AbstractEntity
{
    @Column
    private String firstname;

    @Column
    private String lastname;

    public Contact() {}

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname( String firstname )
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname( String lastname )
    {
        this.lastname = lastname;
    }
}
