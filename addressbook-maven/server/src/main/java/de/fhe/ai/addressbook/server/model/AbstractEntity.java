package de.fhe.ai.addressbook.server.model;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@Inheritance( strategy = InheritanceType.TABLE_PER_CLASS )
public abstract class AbstractEntity
{
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Version
    private Long version;

    @Column
    private Date created;

    @Column
    private Date modified;

    public AbstractEntity() {}

    @PrePersist
    void prePersist()
    {
        this.created = new Date();
    }

    @PreUpdate
    void preUpdate()
    {
        this.modified = new Date();
    }

    public Long getId()
    {
        return id;
    }

    public void setId( Long id )
    {
        this.id = id;
    }

    public Long getVersion()
    {
        return version;
    }

    public void setVersion( Long version )
    {
        this.version = version;
    }

    public Date getCreated()
    {
        return created;
    }

    public void setCreated( Date created )
    {
        this.created = created;
    }

    public Date getModified()
    {
        return modified;
    }

    public void setModified( Date modified )
    {
        this.modified = modified;
    }
}
