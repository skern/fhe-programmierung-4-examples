package de.fhe.ai.addressbook.server.database;

import de.fhe.ai.addressbook.server.interceptor.Logging;
import de.fhe.ai.addressbook.server.model.Contact;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Logging
public class ContactDao
{
    @PersistenceContext( name = "AdressbookPersistenceUnit" )
    EntityManager em;

    public List<Contact> getAllContacts()
    {
        List<Contact> contactList = new ArrayList<>();

        Contact c = new Contact();
        c.setFirstname( "Max" );
        c.setLastname( "Mustermann" );

        em.persist( c );

        long contactId = c.getId();

        Contact c2 = em.find( Contact.class, contactId );

        contactList.add( c2 );

        return contactList;
    }
}
