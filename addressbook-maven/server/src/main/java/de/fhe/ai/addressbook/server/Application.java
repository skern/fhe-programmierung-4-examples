package de.fhe.ai.addressbook.server;

import de.fhe.ai.addressbook.server.controller.AddressbookResource;

import javax.ws.rs.ApplicationPath;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 23.06.15
 * Time: 12:13
 */
@ApplicationPath( "api" )
public class Application extends javax.ws.rs.core.Application
{
    public Application( ) {}


    @Override
    public Set<Class<?>> getClasses( )
    {
        final Set<Class<?>> returnValue = new HashSet<Class<?>>( );

        returnValue.add( AddressbookResource.class );

        return returnValue;
    }
}