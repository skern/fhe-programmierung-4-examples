package de.fhe.ai.addressbook.server.controller;

import de.fhe.ai.addressbook.server.beans.AddressbookService;
import de.fhe.ai.addressbook.server.database.ContactDao;
import de.fhe.ai.addressbook.server.model.Contact;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 22.06.15
 * Time: 15:58
 */
@Path( "addressbook" )
public class AddressbookResource
{
    @Inject
    private AddressbookService addressBookService;

    @GET
    @Produces( "application/json" )
    public List<Contact> getAllEntries()
    {
        return this.addressBookService.getAllContacts();
    }

}
