package de.fhe.ai.addressbook.server.interceptor;

import javax.annotation.Priority;
import javax.interceptor.AroundConstruct;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.logging.Logger;

@Priority(Interceptor.Priority.APPLICATION+10)
@Interceptor
@Logging
public class LoggingInterceptor
{
    private static Logger LOGGER = Logger.getLogger( LoggingInterceptor.class.getSimpleName() );


    @AroundInvoke
    public Object logInvoke(InvocationContext context) throws Exception
    {
        String target = context.getTarget().getClass().getSimpleName();
        String name = context.getMethod().getName();

        LOGGER.info( "Calling " + target + "." + name );

        Object result = context.proceed();

        LOGGER.info( "Finished " + target + "." + name );

        return result;
    }

    @AroundConstruct
    public Object logInit(InvocationContext context) throws Exception
    {
        Object result = context.proceed();

        String target = context.getTarget().getClass().getSimpleName();
        LOGGER.info( "Created " + target );

        return result;
    }
}
